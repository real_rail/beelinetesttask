package ru.beeline.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "sales_point")
public class SalesPointDo {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String name;

    @Transient
    private String city;

    private String address;

    @ManyToOne
    @JoinColumn(name="city_id", referencedColumnName="id")
    @JsonIgnore
    private City cityObject;

    @Transient
    public String getCity() {
        return cityObject.getName();
    }
}
