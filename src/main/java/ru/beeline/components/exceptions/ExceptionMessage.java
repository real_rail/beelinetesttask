package ru.beeline.components.exceptions;

import java.io.Serializable;

class ExceptionMessage implements Serializable {
    private String errorMessage;

    public ExceptionMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}