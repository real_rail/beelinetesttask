package ru.beeline.components.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

public class SalesPointNotFoundException extends RuntimeException {

    public SalesPointNotFoundException() {
        super("Не удалось найти торговые точки для данного города");
    }
}