package ru.beeline.components.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestControllerAdvice
public class SalesPointNotFoundAdvice {

    @ExceptionHandler(SalesPointNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    ExceptionMessage handleException(SalesPointNotFoundException ex) {
        return new ExceptionMessage(ex.getMessage());
    }
}