package ru.beeline.components;

import ru.beeline.models.SalesPointDo;

import java.io.Serializable;
import java.util.List;

public class SalesPointData implements Serializable {
    private List<SalesPointDo> spList;

    public SalesPointData(List<SalesPointDo> spList) {
        this.spList = spList;
    }

    public List<SalesPointDo> getSpList() {
        return spList;
    }
}
