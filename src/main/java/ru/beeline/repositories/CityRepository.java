package ru.beeline.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.beeline.models.City;

public interface CityRepository extends CrudRepository<City, Long> {
    City getCityByName(String name);
}
