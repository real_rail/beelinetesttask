package ru.beeline.repositories;

import org.springframework.data.repository.CrudRepository;
import ru.beeline.models.City;
import ru.beeline.models.SalesPointDo;

import java.util.List;

public interface SalesPointRepository extends CrudRepository<SalesPointDo, Long> {
    List<SalesPointDo> getAllByCityObject(City city);
}
