package ru.beeline.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.beeline.components.SalesPointData;
import ru.beeline.components.exceptions.SalesPointNotFoundException;
import ru.beeline.models.City;
import ru.beeline.models.SalesPointDo;
import ru.beeline.services.CityServiceImpl;
import ru.beeline.services.SalesPointServiceImpl;
import ru.beeline.services.external.IpToRusCityApi;
import ru.beeline.services.external.YandexMapsApi;
import javax.servlet.http.HttpServletRequest;

import java.util.List;

@RequestMapping("/salesPoints")
@RestController
public class SalesPointController {

    private final SalesPointServiceImpl salesPointService;
    private final CityServiceImpl cityService;

    public SalesPointController(SalesPointServiceImpl salesPointService, CityServiceImpl cityService) {

        this.salesPointService = salesPointService;
        this.cityService = cityService;
    }

    @GetMapping()
    SalesPointData getByCoordinates(
            HttpServletRequest request,
            @RequestParam(value = "lat", defaultValue = "0.0") double latitude,
            @RequestParam(value = "long", defaultValue = "0.0") double longitude
    ) throws Exception {

        String cityName, ipAddress = request.getRemoteAddr();
        if (longitude == 0.0 && latitude == 0.0) {
            try {
                cityName = new IpToRusCityApi().GetCityNameByIP(ipAddress);
            } catch (Exception e) {
                throw new Exception("Ошибка отправки запроса в ip2ruscity");
            }
        } else {
            try {
                cityName = new YandexMapsApi().GetCityNameByCoordinates(longitude, latitude);
            } catch (Exception e) {
                throw new Exception("Ошибка отправки запроса в Яндекс.Карты");
            }
        }

        if (cityName == null || cityName.equals("")) {
            throw new Exception("Ошибка определения города клиента");
        }

        City city;
        try {
            city = cityService.getCityByName(cityName);
        } catch (Exception e) {
            throw new Exception("Ошибка поиска города в БД");
        }

        if (city == null) {
            throw new SalesPointNotFoundException();
        }

        List<SalesPointDo> points = salesPointService.getAllByCityObject(city);
        if (points.size() == 0) {
            throw new SalesPointNotFoundException();
        }

        return new SalesPointData(points);
    }
}
