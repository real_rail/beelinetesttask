package ru.beeline.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.beeline.models.City;
import ru.beeline.models.SalesPointDo;
import ru.beeline.repositories.SalesPointRepository;

import java.util.List;

@Service
public class SalesPointServiceImpl implements SalesPointService {

    protected final SalesPointRepository repository;

    @Autowired
    public SalesPointServiceImpl(SalesPointRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<SalesPointDo> getAllByCityObject(City city) {
        return repository.getAllByCityObject(city);
    }
}
