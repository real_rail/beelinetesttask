package ru.beeline.services;

import ru.beeline.models.City;
import ru.beeline.models.SalesPointDo;

import java.util.List;

public interface SalesPointService {
    List<SalesPointDo> getAllByCityObject(City city);
}
