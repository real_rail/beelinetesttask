package ru.beeline.services.external;

import org.apache.http.client.utils.URIBuilder;

import java.net.http.HttpClient;
import java.net.http.HttpRequest;

import java.net.URISyntaxException;
import java.net.http.HttpResponse;
import java.time.Duration;

public class YandexMapsApi {

    private static final String apiKey = "ce0607bf-a42b-4bd6-9461-767aff866263";
    private static final String link = "https://geocode-maps.yandex.ru/1.x/";

    private final HttpClient httpClient = HttpClient.newBuilder()
            .version(HttpClient.Version.HTTP_2)
            .build();

    public String GetCityNameByCoordinates(double latitude, double longitude) throws Exception {

        URIBuilder builder;

        try {
            builder = new URIBuilder(link);
        } catch (URISyntaxException e) {
            throw new Exception("Некорректный адрес ссылки");
        }

        builder.setParameter("apikey", apiKey)
                .setParameter("format", "json")
                .setParameter("results", "1")
                .setParameter("kind", "locality")
                .setParameter("geocode", latitude + "," + longitude);

        HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .uri(builder.build())
//                .setHeader("User-Agent", "Java 11 HttpClient Bot") // add request header
                .timeout(Duration.ofSeconds(5))
                .build();

        HttpResponse<String> response = httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        if (response.statusCode() != 200) {
            throw new Exception();
        }

        return "name";
    }
}
