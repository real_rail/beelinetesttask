package ru.beeline.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.beeline.models.City;
import ru.beeline.repositories.CityRepository;

@Service
public class CityServiceImpl implements CityService {

    private final CityRepository repository;

    @Autowired
    public CityServiceImpl(CityRepository repository) {
        this.repository = repository;
    }

    @Override
    public City getCityByName(String name) {
        return repository.getCityByName(name);
    }
}
