package ru.beeline.services;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import ru.beeline.models.City;

@Service
public interface CityService {
    City getCityByName(String name);
}
