package ru.beeline.services.external;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class YandexMapsApiTest {

    private YandexMapsApi api;


    @Before
    public void setApi()
    {
        api = Mockito.spy(YandexMapsApi.class);
    }

    @Test
    public void GetMoscowCoordinates_ShouldBeOK()
    {
        try {
            when(api.GetCityNameByCoordinates(55.780942, 37.591593)).thenReturn("Москва");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void GetTaygaCoordinates_ShouldBeException()
    {
        try {
            when(api.GetCityNameByCoordinates(56.099090, 85.737963)).thenThrow(new Exception());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}