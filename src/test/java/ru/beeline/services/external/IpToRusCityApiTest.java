package ru.beeline.services.external;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import static org.mockito.Mockito.when;

public class IpToRusCityApiTest {
    private IpToRusCityApi api;


    @Before
    public void setApi()
    {
        api = Mockito.spy(IpToRusCityApi.class);
    }

    @Test
    public void GetMoscowIP_ShouldBeOK()
    {
        try {
            when(api.GetCityNameByIP("37.190.39.0")).thenReturn("Москва");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void GetZeroIP_ShouldBeException()
    {
        try {
            when(api.GetCityNameByIP("0.0.0.0")).thenThrow(new Exception());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void GetLocalhostIP_ShouldBeException()
    {
        try {
            when(api.GetCityNameByIP("127.0.0.1")).thenThrow(new Exception());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void GetLocalhostIP2_ShouldBeException()
    {
        try {
            when(api.GetCityNameByIP("192.168.1.100")).thenThrow(new Exception());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}