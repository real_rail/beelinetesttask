package ru.beeline.controllers;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.beeline.components.exceptions.SalesPointNotFoundException;
import ru.beeline.models.City;
import ru.beeline.services.CityService;
import ru.beeline.services.SalesPointService;
import org.springframework.test.web.servlet.MockMvc;
import ru.beeline.services.external.IpToRusCityApi;
import ru.beeline.services.external.YandexMapsApi;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class SalesPointControllerTest {

    private MockMvc mockMvc;

    @MockBean
    private SalesPointService salesPointService;

    @MockBean
    private CityService cityService;

    @MockBean
    private YandexMapsApi yandexMapsApi;

    @Test
    public void getCityByName_Exception_ShouldReturnHttpStatusCode404() throws Exception {

        when(cityService.getCityByName("Нью-Йорк")).thenThrow(new SalesPointNotFoundException());

        mockMvc.perform(get("/salesPoints?lat=40.651263&long=-73.951679"))
                .andExpect(status().isNotFound());

        verify(cityService, times(1)).getCityByName("Нью-Йорк");
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void getVillageByName_Exception_ShouldReturnHttpStatusCode404() throws Exception {

        City kukuevo = cityService.getCityByName("Кукуево");

        when(salesPointService.getAllByCityObject(kukuevo)).thenThrow(new SalesPointNotFoundException());

        mockMvc.perform(get("/salesPoints?lat=54.500776&long=33.008766"))
                .andExpect(status().isNotFound());

        verify(cityService, times(1)).getCityByName("Кукуево");
        verify(salesPointService, times(1)).getAllByCityObject(kukuevo);
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void checkNullIp_Exception_ShouldReturnHttpStatusCode500() throws Exception {

        when(new IpToRusCityApi().GetCityNameByIP("0.0.0.0")).thenThrow(new Exception("Ошибка поиска города в БД"));

        mockMvc.perform(get("/salesPoints?lat=0.0&long=0.0"))
                .andExpect(status().isInternalServerError());

        verify(cityService, times(1)).getCityByName("");
        verifyNoMoreInteractions(cityService);
    }

    @Test
    public void checkBadIpStr_Exception_ShouldReturnHttpStatusCode500() throws Exception {

        when(new IpToRusCityApi().GetCityNameByIP("")).thenThrow(new Exception("Ошибка определения города клиента"));

        mockMvc.perform(get("/salesPoints?lat=0.0&long=0.0"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void checkBadLat_Exception_ShouldReturnHttpStatusCode500() throws Exception {

        when(new YandexMapsApi().GetCityNameByCoordinates(99999999999999999999999999999.9999999999, 99999999999999999999999.999999))
                .thenThrow(new Exception("Ошибка отправки запроса в Яндекс.Карты"));

        mockMvc.perform(get("/salesPoints?lat=99999999999999999999999999999.9999999999&long=99999999999999999999999.999999"))
                .andExpect(status().isInternalServerError());
    }
}